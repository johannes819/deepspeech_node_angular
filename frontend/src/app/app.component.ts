import { Component, OnInit } from '@angular/core';
import { AudioService } from './services/audio.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  private readonly vocabulary: string[] = [
    "new order",
    "cuba libre",
    "gin tonic",
    "long island ice tea",
    "mojito",
    "lynchburg lemonade",
    "submit order",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
    "ten",
  ];


  constructor(
    public audioService: AudioService
  ) { }

  ngOnInit(): void {
  }


  /*###################    Vocabularies     #####################*/

  private isSpeaking: boolean = false;
  speakWordsClicked(){
    if(this.isSpeaking){
      this.isSpeaking= false;
      //stop speaking
    }else{
      this.isSpeaking= true;
      this.fakeWords(0);
    }
  }


  speakOneWord(word:string): SpeechSynthesisUtterance{
    let utter = new SpeechSynthesisUtterance(word);
    // utter.voice = window.speechSynthesis.getVoices()[48];
    utter.rate = 0.8;

    window.speechSynthesis.speak(utter);
    return utter;
  }

  fakeWords = (index)=> {
    if(!this.isSpeaking){
      return;
    }

    if (index == this.vocabulary.length) {
      return;
    }
    let utter= this.speakOneWord(this.vocabulary[index]);
    index++;

    utter.onend = async () => {
      await this.wait(500);
      this.fakeWords(index);
    }
  }

  wait(ms: number){
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
