/*
Note:

This resampler currently only works for one process per instance. Side effects are being used for ease of implementation.
For the example of this app it's not neccesary to run it in paralell
Ideally this should run inside the scope of a WebWorker so it also doesn't affect the main loop
*/

import { Observable, Subject } from 'rxjs';
import { map, takeUntil, bufferTime} from 'rxjs/operators';

let audioContext: AudioContext;
let gumStream: MediaStream;
let resampledFrames$: Subject<Int16Array>;
let streamEndNotifier$: Subject<void>;
let resampledStream$: Observable<Int16Array>;

/**
 * Resamples a AudioStream to 16 KHz
 * @param sourceStream AudioStream from the AudioStreams API
 * @param emitInterval Interval in ms that data should be emitted. Defaults to infinity
 * @returns Returns resampled Stream as an Observable that emits data in intervals or when the input stream has finished
 */
export function resampleStream(sourceStream: MediaStream, emitInterval: number = Number.MAX_SAFE_INTEGER/10): Observable<Int16Array> {
    gumStream = sourceStream;
    resampledFrames$ = new Subject<Int16Array>();

    //TODO: Fix 'as any'
    audioContext = new ((window as any).AudioContext || (window as any).webkitAudioContext)();
    let audioSource = audioContext.createMediaStreamSource(sourceStream);
    let node = audioContext.createScriptProcessor(4096, 1, 1);
    node.onaudioprocess = (e) => {
        processAudioFrame(e.inputBuffer.getChannelData(0),audioSource.context.sampleRate);
    };
    audioSource.connect(node);
    node.connect(audioContext.destination);

    streamEndNotifier$  = new Subject<void>();

    resampledStream$ = resampledFrames$.pipe(
        takeUntil(streamEndNotifier$),
        bufferTime(emitInterval),
        map(mergeInt16Arrays),
    );
    return resampledStream$;
}

/**
 * Merges multiple Int16Arrays to one. In order first->last
 * @param arrays Array of Int16Array's to be merged
 */
export function mergeInt16Arrays(arrays: Int16Array[]): Int16Array {
    return arrays.reduce((previous, current) => {
        return new Int16Array([...previous, ...current]);
    }, new Int16Array())
}

// Temporarily needed for processing
let inputBuffer = [];

/**
 * Processes raw AudioStream frames to 16KHz and emits the resampled frames to the Observable `resampledFrames$` as a side effect.
 * @param inputFrame Frame from the ScriptProcessorNode
 */
function processAudioFrame(inputFrame, inputSampleRate: number) {
    for (let i = 0; i < inputFrame.length; i++) {
        inputBuffer.push((inputFrame[i]) * 32767);
    }

    const goalSampleRate = 16000;
    const frameLength = 512;

    while ((inputBuffer.length * goalSampleRate / inputSampleRate) > frameLength) {
        let outputFrame = new Int16Array(frameLength);
        let sum = 0;
        let num = 0;
        let outputIndex = 0;
        let inputIndex = 0;

        while (outputIndex < frameLength) {
            sum = 0;
            num = 0;
            while (inputIndex < Math.min(inputBuffer.length, (outputIndex + 1) * inputSampleRate / goalSampleRate)) {
                sum += inputBuffer[inputIndex];
                num++;
                inputIndex++;
            }
            outputFrame[outputIndex] = sum / num;
            outputIndex++;
        }

        resampledFrames$.next(outputFrame);
        inputBuffer = inputBuffer.slice(inputIndex);
    }
}

/**
 * Should be called when the Recording has ended.
 * Finalizes the resampling process and stops/resets all nessesary values
 */
export function stopResampling() {
    //emit last ping since intervall won't ping last time at say 800ms 
    streamEndNotifier$.next(); 
    audioContext.close();
    gumStream.getAudioTracks()[0].stop();
    inputBuffer = [];
}
