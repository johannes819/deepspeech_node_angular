import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Subscription } from "rxjs";
import { resampleStream, stopResampling, mergeInt16Arrays } from "../helpers/resampler";
import { AudioService } from '../services/audio.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-static-recorder',
  templateUrl: './static-recorder.component.html',
  styleUrls: ['./static-recorder.component.less']
})
export class StaticRecorderComponent implements OnInit {

  constructor(
    public audioService: AudioService
  ) { }

  public recordingState$: BehaviorSubject<string> = new BehaviorSubject('inactive');

  public results: any[] = [];

  private data: Int16Array;

  ngOnInit(): void {
  }

  async startRecording() {
    if (this.recordingState$.value === 'inactive') {
      let audioStream = await this.audioService.getUserMediaStream();

      this.recordingState$.next("recording");

      this.data = new Int16Array();
      resampleStream(audioStream).subscribe((data) => {
        this.data = new Int16Array([...this.data, ...data]);
      });
    }
  }

  async stopRecording() {
    if (this.recordingState$.value === 'recording') {
      stopResampling();
      this.recordingState$.next('inactive');

      let metadataResult = await this.audioService.getTextFromAudio(this.data);
      if (metadataResult.transcription !== "") {
        this.results.push(metadataResult);
      }
    }
  }

  resetResults() {
    this.results = [];
  }
}
