import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticRecorderComponent } from './static-recorder.component';

describe('StaticRecorderComponent', () => {
  let component: StaticRecorderComponent;
  let fixture: ComponentFixture<StaticRecorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticRecorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticRecorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
