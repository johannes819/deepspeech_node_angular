import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable, BehaviorSubject } from "rxjs";

export interface IMetadataTranscription {
  confidence: number;
  transcription: string;
}

/**
 * 
 */
export interface IStreamTranscription {
  transcription: string;
  finished: boolean;
}


@Injectable({
  providedIn: 'root'
})
export class AudioService {

  constructor(private http: HttpClient) { }

  public getTextFromAudio(binaryData: Int16Array): Promise<IMetadataTranscription> {
    return this.http.post<IMetadataTranscription>("/api/audio", binaryData.buffer, { responseType: 'json' }).toPromise()
  }

  public getUserMediaStream(): Promise<MediaStream> {
    const constraints: any = {
      audio: {
        "noiseSuppression": true
      }
    };

    return navigator.mediaDevices.getUserMedia(constraints);
  }

  private lmType$ = new BehaviorSubject<string>("cocktail_modified")

  public getLanguageModelType(): Observable<string> {
    return this.lmType$.asObservable();
  }

  public setLanguageModelType(modelType: string) {
    this.http.post("api/languagemodel", { modelType }).subscribe(() => {
      this.lmType$.next(modelType);
    });
  }
}
