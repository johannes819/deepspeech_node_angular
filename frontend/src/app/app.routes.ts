import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { VadStreamingComponent } from './vad-streaming/vad-streaming.component';
import { StaticRecorderComponent } from './static-recorder/static-recorder.component';

export const routes: Routes = [
  { path: 'stream', component: VadStreamingComponent },
  { path: 'recorder', component: StaticRecorderComponent },
  { path: '**', redirectTo: 'recorder' }
];