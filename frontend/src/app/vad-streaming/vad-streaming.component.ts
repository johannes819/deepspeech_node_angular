import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { BehaviorSubject, Subject } from "rxjs";
import { AudioService, IStreamTranscription } from '../services/audio.service';


@Component({
  selector: 'app-vad-streaming',
  templateUrl: './vad-streaming.component.html',
  styleUrls: ['./vad-streaming.component.less']
})
export class VadStreamingComponent implements OnInit, OnDestroy {

  constructor(
    private sanitizer: DomSanitizer,
    private zone: NgZone,
    public audioService: AudioService
  ) { }

  public mediaRecorder: MediaRecorder;
  public state$: BehaviorSubject<string> = new BehaviorSubject('inactive');
  public ws: WebSocket;
  public gumStream: MediaStream;

  public results: IStreamTranscription[] = [];

  // Needed to accumulate the data Client side to generate a final BLOB when stop streaming
  private collectedBlob: Blob;
  private finalUrl: SafeResourceUrl;

  ngOnInit(): void {
    this.startRecording();
  }
  ngOnDestroy(){
    this.stopRecording();
  }

  setupWebsocketConnection(): Promise<void> {
    let isOpen = new Subject<void>();

    this.ws = new WebSocket("ws://localhost:3000");
    this.ws.onopen = () => {
      console.info("Connection is open");
      isOpen.complete();
    };

    this.ws.onmessage = e => this.handleMessageFromWs(e.data);

    this.ws.onclose = () => console.info("Websocket connection is closed");

    return isOpen.toPromise();
  }

  appendToBlob(original, newBlob): Blob {
    if (original == null) {
      return newBlob;
    } else {
      return new Blob([original, newBlob], { type: "audio/webm" });
    }
  }

  async startRecording() {
    this.state$.next('recording');
    this.collectedBlob = null;
    await this.setupWebsocketConnection();
    this.gumStream = await this.audioService.getUserMediaStream();
    this.mediaRecorder = new MediaRecorder(this.gumStream);
    this.mediaRecorder.start(1000);
    //first wait until websocket connection is set up then register ondataHandler but start recording before and 
    this.mediaRecorder.ondataavailable = (e) => {
      //zone.js is not MonkeyPatched for the MediaStream & MediaRecorder API, let zone run this to trigger angulars change detection
      this.zone.run(() => {
        //Only new data since last ondataavailable event
        //send over WS and create if not yet existing
        if (this.state$.value == "recording") {
          this.ws.send(e.data);
          this.collectedBlob = this.appendToBlob(this.collectedBlob, e.data);
        }
      })
    }
  }

  stopRecording() {
    if (this.state$.value === 'recording') {
      console.log("stopped recording");
      // Trigger another last ondataavailable event since some new data is avalable but the interval won't get to trigger it since the recording is being stopped before.
      this.gumStream.getTracks().forEach(track => track.stop());

      //This terminates the stream server side by setting EOF
      this.state$.next('inactive');
      this.ws.close();

      //Finish last running transcription manually because VAD didn't register end of speech
      if (this.results.length > 0) {
        this.results[this.results.length - 1].finished = true;
      }

      if (this.collectedBlob) {
        this.finalUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(this.collectedBlob));
      }
    }
  }

  // Handles the incoming data from the Websocket
  handleMessageFromWs(msg: string) {
    const parsedMessage: IStreamTranscription = JSON.parse(msg);
    if (parsedMessage.transcription !== "") {
      if (this.results.length == 0 || this.results[this.results.length - 1].finished == true) {
        this.results.push(parsedMessage)
      } else {
        this.results[this.results.length - 1] = parsedMessage;
      }
      //keep scrolling down to see latest list entry
      window.scrollTo(0, document.body.scrollHeight);
    }
  }

  resetResults() {
    this.results = [];
  }
}
