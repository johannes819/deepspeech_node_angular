import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VadStreamingComponent } from './vad-streaming.component';

describe('VadStreamingComponent', () => {
  let component: VadStreamingComponent;
  let fixture: ComponentFixture<VadStreamingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VadStreamingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VadStreamingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
