# Deepspeech example app using Node.js and Angular (By W11K)
![](./utils/example_app.png)  

This Project was created as the practical implementation of a bachelor thesis by [W11k GmbH](https://www.w11k.de/).  
The user is able to speak into the microphone and get the spoken audio converted into text, either by recording or streaming it.  

# Setup
- Install node.js
- `npm install` in both `./backend` and `./frontend`
- Install ffmpeg. Read [here](https://github.com/adaptlearning/adapt_authoring/wiki/Installing-FFmpeg) how to -  Its used by node to spawn a child process to convert the audiostream to 16KHz mono-channel s16le PCM 

# Run
- `npm start` inside `./frontend`
- `node server` inside `./backend`

# Usage
Go to your browser of choice (chrome) navigate to `localhost:4200` and use the start/stop buttons to stream the audio to the server. It will then continuously respond with the transcribed text as soon as it detected a word.

**Note** that you will be asked to allow microphone acces in form of a browser-popup.

You can switch between the default and a custom language model in the application.


# Extra knowledge
ffmpeg is used server-side to parse the AudioStream it receives from the browser via a websocket connection to a `s16le PCM` formatted stream which is needed for further processing (VAD).

The default 0.6.0 pre-trained acoustic model was used in this example.

The language Model was created with the following script which takes an `alphabet.txt` and `vocabulary.txt` and outputs a `words.arpa`, `trie` and `lm.binary` file:

```
../kenlm/build/bin/lmplz --text training_material/vocabulary.txt --arpa output_files/words.arpa --order 5 --discount_fallback --temp_prefix /tmp/
../kenlm/build/bin/build_binary -T -s trie output_files/words.arpa output_files/lm.binary
../native_client.amd64.cpu.osx/generate_trie training_material/alphabet.txt output_files/lm.binary output_files/trie
```

# Bugs
When reloading the browser you have to choose the language-model again or the last chose one will be taken.
A lot of things are currently still very ugly and not well coded but this project is still work-in-progress and to already make it work a lot of shortcuts were taken. This doesn't affect the accuracy though.