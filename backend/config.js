const VAD = require("node-vad");

module.exports = {
    model: __dirname + '/models/0.6.x/output_graph.pbmm',
    // Time in milliseconds for debouncing speech active state -- Default 20
    DEBOUNCE_TIME: 20,

    // Default is 16kHz
    AUDIO_SAMPLE_RATE: 16000,

    // Beam width used in the CTC decoder when building candidate transcriptions
    BEAM_WIDTH: 500,

    // The alpha hyperparameter of the CTC decoder. Language Model weight. Default: 0.75
    LM_ALPHA: 0.75,

    // The beta hyperparameter of the CTC decoder. Word insertion bonus. Default: 1.85
    LM_BETA: 1.85,

    // Defines different thresholds for voice detection
    // NORMAL: Detection mode with lowest miss-rate. Works well for most inputs.
    // LOW_BITRATE: Detection mode optimised for low-bitrate audio.
    // AGGRESSIVE: Detection mode best suited for somewhat noisy, lower quality audio.
    // VERY_AGGRESSIVE: Suitable for high bitrate, low-noise data. May classify noise as voice, too.
    VAD_MODE: VAD.Mode.NORMAL 
}