const Ds = require('deepspeech');
const { Readable } = require('stream');
var cors = require('cors');
var express = require('express');
const { spawn } = require('child_process');
const WebSocket = require('ws');
const VAD = require("node-vad");
const config = require('./config');

// ############ Global variables   ############
let websocket;
let audioStream;
let ffmpeg;
let lmType;
let model;
let sctx;

var app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const server = app.listen(3000, () => {
    console.log('Webserver listening on port 3000!');
});

app.post('/languagemodel', (req, res) => {
    setupDeepspeechModels(req.body.modelType);
    res.send({ success: true, lmConfig: req.body.modelType });
});

app.post('/audio', (req, res) => {
    let chunks = [];
    req.on('data', (chunk) => {
        chunks.push(chunk)
    });
    req.on('end', () => {
        let buffer = Buffer.concat(chunks);
        let result = model.sttWithMetadata(buffer.slice(0, buffer.length / 2));

        let transcription = result.items.reduce((acc, curr) => {
            return acc + curr.character
        }, "");

        let probability = Math.exp(result.confidence) / (1 + Math.exp(result.confidence));
        console.log('transcription:', transcription);

        res.send({
            confidence: probability,
            transcription: transcription
        });
    });
});

// ############      ############

function finishStream() {
    let result = model.finishStream(sctx);
    
    // Currently the 0.6.0 model incorrectly infers silence to "i", so this is a workaround or it. VAD not only sends the detected word but also the detected silence.
    //Reference to Issue: https://github.com/mozilla/DeepSpeech/issues/2579
    if (result !== "") {
        websocket.send(JSON.stringify({
            transcription: result,
            finished: true
        }));
        console.log('Transcription: ', result);
    }
}

function intermediateDecode() {
    finishStream();
    sctx = model.createStream();
}

function feedAudioContent(chunk) {
    let sliced = chunk.slice(0, chunk.length / 2);
    model.feedAudioContent(sctx, sliced);
}

function processVad(data) {
    if (data.speech.start || data.speech.state) {
        feedAudioContent(data.audioData);
        let result = model.intermediateDecode(sctx);
        if (result !== "i" && result !== "ten" && result !== "") {
            websocket.send(JSON.stringify({
                transcription: result,
                finished: false
            }));
        }
    }
    else if (data.speech.end) {
        feedAudioContent(data.audioData);
        intermediateDecode()
    }
}

/**
 * Creates the Model for deepspeech and sets it up with a given language-model type
 * @param {string} lmType type of the language Model to be loaded. Available: default, cocktail
 */
function setupDeepspeechModels(newType) {
    if (newType != lmType) {
        lmType = newType;
        model = new Ds.Model(config['model'], config.BEAM_WIDTH);
        sctx = model.createStream();
        console.error('Loading model from file %s', config.model);
        if(newType !== "none"){
            console.error('Loading language model %s', newType);
            model.enableDecoderWithLM(__dirname + '/models/0.6.x/' + newType + '/lm.binary', __dirname + '/models/0.6.x/' + newType + '/trie', config.LM_ALPHA, config.LM_BETA);
        }else{
            console.error("No language model loaded");
        }
    }
}


function setupStreamProcessing() {
    audioStream = new Readable({
        // The read logic is omitted since the data is pushed to the socket
        // outside of the script's control. However, the read() function 
        // must be defined.
        read() { }
    });

    // ################   FFMPEG    ##############

    ffmpeg = spawn('ffmpeg', [
        '-hide_banner',
        '-nostats',
        //'-loglevel', 'fatal',
        '-i', '-',
        '-vn',
        '-acodec', 'pcm_s16le',
        '-ac', 1,
        '-ar', config.AUDIO_SAMPLE_RATE,
        '-f', 's16le',
        'pipe:1'
    ]);

    ffmpeg.stderr.on("data", (data, err) => {
        //console.info("stdout", data.toString());
        if (err) {
            console.error(err);
        }
    });

    audioStream.pipe(ffmpeg.stdin);

    // ################   VAD    ##############

    const VAD_STREAM = VAD.createStream({
        mode: config.VAD_MODE,
        audioFrequency: config.AUDIO_SAMPLE_RATE,
        debounceTime: config.DEBOUNCE_TIME
    });

    ffmpeg.stdout.pipe(VAD_STREAM).on('data', processVad);
};

function setupWebsocketConnection() {
    const websocketServer = new WebSocket.Server({ server });
    websocketServer.on('connection', (ws) => {
        console.info("WebSocket connection opened")
        websocket = ws;
        setupStreamProcessing();

        websocket.on('message', (data) => {
            audioStream.push(data);
        });

        websocket.on('close', () => {
            //Undo all of streampipeline and chilProcess
            audioStream.push(null); //Closes the stream
            ffmpeg.kill()
            console.info("WebSocket connection closed")
        })
    });
}

setupDeepspeechModels("cocktail_modified");
setupWebsocketConnection();
